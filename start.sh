#!/bin/bash

#------------------------------------------------------------------
# General configs
#------------------------------------------------------------------
FACTORY="CERN-ITB"
ENTRY="CMSHTPC_T2_CH_CERN_ce511"
SCHEDD="schedd_glideins7@vocms0204.cern.ch"
WEB="http://vocms0204.cern.ch/factory/stage"
PARAM_GLIDEICLIENT_REQNODE="vocms0204.dot,cern.dot,ch"

CLIENTGROUP="test"
CLIENTNAME="CMSG-ITB_gWMSFrontend-v1_0."$CLIENTGROUP
CLIENTWEB="http://vocms0802.cern.ch/vofrontend/stage"
PARAM_GLIDEIN_COLLECTOR="vocms0809.dot,cern.dot,ch.colon,9620.minus,9645"
CLIENTWEBGROUP="http://vocms0802.cern.ch/vofrontend/stage/group_"$CLIENTGROUP

#------------------------------------------------------------------
# Factory checksums
#------------------------------------------------------------------
#Get this info by executing the following script at the factory:
# ./get_factory_checksums.sh ENTRY_NAME
#  e.g. ./get_factory_checksums.sh CMSHTPC_T2_CH_CERN_ce511
DESCRIPT="description.i9jceL.cfg"
SIGN="0328388d474dcfe115acee546067e6ef264c8475"
SIGNENTRY="96d53cc4ea5271a9e15baf8f6c61ce20bacfa002"

#------------------------------------------------------------------
# Frontend checksums
#------------------------------------------------------------------
#Get this info by executing the following script at the frontend:
# ./get_frontend_checksums.sh GROUP_NAME
# e.g. ./get_frontend_checksums.sh test

CLIENTDESCRIPT="description.i9ihnP.cfg"
CLIENTSIGN="53c89e695c4b0cc20b861f911c667545fcbfceb3"
CLIENTSIGNGROUP="2e9fda343b26a52398920f73ddbbc77a7191f4ec"

#------------------------------------------------------------------

args="source ./setenv.source; ./glidein_startup$UBER.sh \
-v std \
-name gfactory_instance \
-entry $ENTRY \
-clientname $CLIENTNAME \
-schedd $SCHEDD \
-proxy None \
-factory $FACTORY \
-web $WEB \
-sign $SIGN \
-signentry $SIGNENTRY \
-signtype sha1 \
-descript $DESCRIPT \
-descriptentry $DESCRIPT \
-dir Condor \
-param_GLIDEIN_Client $CLIENTNAME \
-submitcredid 391162 \
-slotslayout fixed \
-clientweb $CLIENTWEB  \
-clientsign $CLIENTSIGN \
-clientsigntype sha1 \
-clientdescript $CLIENTDESCRIPT \
-clientgroup $CLIENTGROUP \
-clientwebgroup $CLIENTWEBGROUP \
-clientsigngroup $CLIENTSIGNGROUP \
-clientdescriptgroup $CLIENTDESCRIPT \
-param_CONDOR_VERSION 8.dot,6.dot,11 \
-param_GLIDEIN_Glexec_Use OPTIONAL \
-param_CMS_GLIDEIN_VERSION 15 \
-param_GLIDEIN_Job_Max_Time 34800 \
-param_GLIDECLIENT_ReqNode $PARAM_GLIDEICLIENT_REQNODE \
-param_CONDOR_OS default \
-param_MIN_DISK_GBS 1 \
-param_GLIDEIN_Max_Idle 600 \
-param_GLIDEIN_Monitoring_Enabled False \
-param_CONDOR_ARCH default \
-param_UPDATE_COLLECTOR_WITH_TCP True \
-param_GLIDEIN_Max_Tail 600 \
-param_USE_MATCH_AUTH True \
-param_GLIDEIN_Report_Failed NEVER \
-param_GLIDEIN_Collector $PARAM_GLIDEIN_COLLECTOR \
-param_GLIDEIN_CCB vocms0816.dot,cern.dot,ch.colon,9619.minus,9644 
-cluster 280 \
-subcluster 0"

exec sudo -H -u condor bash -c "$args"

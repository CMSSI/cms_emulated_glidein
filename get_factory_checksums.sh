ENTRY_NAME=$1

# Getting the sha1 key for the latest version of all config files
last_sha1=`ls /var/lib/gwms-factory/web-area/stage/signature.* | tail -1 | awk -F "." {'print $2'}`

# Getting the Factory signature
factory_signature=`sha1sum /var/lib/gwms-factory/web-area/stage/signature.$last_sha1.sha1 | awk {'print $1'}`

# Getting the Entry signature
#TODO
#    check that ENTRY_NAME is defined 
#    check that  /var/lib/gwms-factory/web-area/stage/entry_$ENTRY_NAME/ exists
entry_signature=`sha1sum /var/lib/gwms-factory/web-area/stage/entry_$ENTRY_NAME/signature.$last_sha1.sha1 | awk {'print $1'}`
 

echo "-----------------------------------------------------"
echo "DESCRIPT=\"description."$last_sha1".cfg\""
echo "SIGN=\""$factory_signature"\""
echo "SIGNENTRY=\""$entry_signature"\""
echo "-----------------------------------------------------"
